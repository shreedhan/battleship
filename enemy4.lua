-- Enemy - Boss
local enemy = require("enemy")
local audioTable = require("audioTable")
-- Define properties for enemy4
local enemy4 = {
	name = "enemy4",
	health = 100,
	entrySound = audioTable.enemy4Enter,
	fireSound = audioTable.enemy4Fire,
	speed = 0.1,
	seqData = {{ name="normal", frames={6} } },
	outlineFrameIndex = 6,
	fireRate = 2,
}

enemy4 = enemy:new(enemy4)

return enemy4