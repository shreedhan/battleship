--explosion

local audioTable = require("audioTable");
local sheet = require("sheet")

local explosion = {
	name = "explosion",
	sound = audioTable.explosion,
	seqData = {
		{ name = "normal", frames = {12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27}, time = 1000,}
	},
};

--Constructor for the explosion class
function explosion:new(o)
	o = o or {};
	setmetatable( o, self);
	self.__index = self;
	return o;
end 

--function to generate explosion sequence
function explosion:spawn()
	local sheet = sheet:createImageSheet();
	local explosion = display.newSprite( sheet, self.seqData );
	explosion.x = self.xPos;
	explosion.y = self.yPos;
	explosion:play();
	local explosionSound = audio.loadSound( self.sound )
	self.soundChannel = audio.play(explosionSound, {loops = 1})
	self.dispObj = explosion;
	timer.performWithDelay( 1000, function (event) explosion:removeSelf(); explosion = nil; audio.stop(self.soundChannel); end )
	return self;
end

return explosion;