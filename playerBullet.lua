--represents the bullet fired by player
local bullet = require("bullet")
local collisionFilters = require("collisionFilters")
local audioTable = require("audioTable")

local playerBullet = bullet:new({
		name="playerBullet",
		collisionFilter = collisionFilters.playerBullet,
		sound = audioTable.playerBulletFire1,
	})

return playerBullet