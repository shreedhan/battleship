--represents level 2 of the game
local composer = require("composer")
local stage = require("stage")
local const = require("const")
local deepcopy = require("deepcopy")

local scene = composer.newScene()
local level2
local sceneGroup

function createBackground(sceneGroup)
	local bg, cloud, staticCloud1, staticCloud2
	bg = display.newImage( "images/sea2.jpg", display.contentCenterX, display.contentCenterY + 150 );
	cloud = display.newImage( "images/cloud.png", display.contentCenterX - 150, display.contentCenterY);
	staticCloud1 = display.newImage( "images/cloud.png", display.contentCenterX - 150, display.contentCenterY - 250 );
	staticCloud2 = display.newImage( "images/cloud.png", display.contentCenterX + 250, display.contentCenterY - 150);
	staticCloud1.alpha = 0.5
	staticCloud2.alpha = 0.7
	staticCloud2.xScale = 0.5
	staticCloud2.yScale = 0.5
	sceneGroup:insert(bg)
	sceneGroup:insert(cloud)
	sceneGroup:insert(staticCloud1)
	sceneGroup:insert(staticCloud2)
	Runtime:addEventListener("enterFrame", function ( event )
		cloud.x = cloud.x - 0.7
		if cloud.x <= -100 - cloud.contentWidth/2 then
			cloud.x = display.contentWidth + cloud.contentWidth/2 + 100
			cloud.xScale = math.random(20, 100) / 100
		end
	end)
end

function scene:create( event )
	local sceneGroup = self.view
	createBackground(sceneGroup)
end

local function gotoNextLevel()
	composer.gotoScene("level3",{params = {score=level2.gameState.score}})
end 

--function to reset the game variables to initial values
function resetVariables()
	level2.enemyCount = const.level2.enemyCount
	level2.enemies = table.deepcopy(level2._enemies)
	level2.enemyTypes = const.level2.enemyTypes
	level2.enemyInterval = const.level2.enemyInterval
	level2.gameState = {score = level2.pScore, enemyDestroyed = 0, playerHealth = const.level2.playerHealth, }
				
end 

--function to check if the game is finished
local function checkGameStatus( event )
	--if all enemies are finished, player advances to next level
	if level2.gameState.enemyDestroyed == level2.enemyCount then
		Runtime:removeEventListener( "enterFrame", checkGameStatus )
		level2:removeListeners()
		timer.performWithDelay( 1000, function(event)
			local nextLevel = display.newImage("images/btnNext.png", display.contentCenterX, display.contentCenterY - 80)
			sceneGroup:insert(nextLevel)
			
			local prevLevel = display.newImage("images/btnPlayAgain.png", display.contentCenterX, display.contentCenterY -40)
			sceneGroup:insert(prevLevel)

			local quitGame = display.newImage("images/btnQuit.png", display.contentCenterX, display.contentCenterY )
			sceneGroup:insert(quitGame)

			nextLevel:addEventListener( "tap", function (event) 
				nextLevel:removeSelf( )
				prevLevel:removeSelf( )
				quitGame:removeSelf( )

				gotoNextLevel(event)
			end)

			prevLevel:addEventListener( "tap", function ( event )
				nextLevel:removeSelf( )
				prevLevel:removeSelf( )
				quitGame:removeSelf( )

				level2:clean()
				resetVariables()
			
				level2:initializeEnemy()
				level2:initializePlayer()
				level2:updateHealth()
				level2:updateScore()

				-- Add event listener for overlay
				level2:addListeners()
				Runtime:addEventListener( "enterFrame", checkGameStatus )				
			end )

			quitGame:addEventListener( "tap", function (event)
				composer.gotoScene("end", {params={level=level2.name,score=level2.gameState.score}})
			end)
		end)
	--if player has no remaining health, destroy the player 
	elseif (level2.gameState.playerHealth <= 0) then
		Runtime:removeEventListener( "enterFrame", checkGameStatus )
		level2:explodePlayer(event)
		level2:cleanAllEnemies()
		resetVariables()
		timer.performWithDelay( 1000, function(event)
			level2.startPlay = display.newImage("images/btnPlayAgain.png", display.contentCenterX, display.contentCenterY)
			level2:resetPlay()

			local quitGame = display.newImage("images/btnQuit.png", display.contentCenterX, display.contentCenterY + 50)
			sceneGroup:insert(quitGame)

			quitGame:addEventListener( "tap", function (event)
				level2.startPlay.isVisible = false
				quitGame:removeSelf( )
				composer.gotoScene("end", {params={level=level2.name,score=level2.gameState.score}})
			end)

			Runtime:addEventListener( "enterFrame", checkGameStatus )
		end)
	end
end

function scene:show( event )
	sceneGroup = self.view
	if (event.phase == "did") then
		-- moveBackground(1)
		level2 = stage:new({
				name = const.level2.name,
				enemyCount = const.level2.enemyCount,
				enemies = const.level2.enemies,
				enemyTypes = const.level2.enemyTypes,
				enemyInterval = const.level2.enemyInterval,
				sceneGroup = sceneGroup,
				gameState = {score = event.params.score, enemyDestroyed = 0, playerHealth = const.level2.playerHealth, },
			})
		level2.pScore = event.params.score
		level2:create()
		Runtime:addEventListener( "enterFrame", checkGameStatus )
	end
end

function scene:hide(event)
	if(event.phase == "will") then
		if(level2.player ~= nil) then
			level2.player:clean()
		end 
	end
	level2:cleanAllEnemies()
end 

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )

return scene