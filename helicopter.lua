-- Enemy - Helicopter
local enemy = require("enemy")
local audioTable = require("audioTable")

-- Properties for helicopter
local helicopter = {
	name = "helicopter",
	health = 5,
	entrySound = audioTable.helicopterEnter,
	fireSound = audioTable.helicopterFire,
	speed = 0.1,
	seqData = {{ name="normal", frames={8,9,10,11}, time=100 }, },
	fireRate = 0.8,
	outlineFrameIndex = 8,
}

helicopter = enemy:new(helicopter)

return helicopter