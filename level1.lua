--represents level 1 of the game
local composer = require("composer")
local stage = require("stage")
local const = require("const")
local deepcopy = require("deepcopy")

local scene = composer.newScene()
local level1
local sceneGroup

function createBackground(sceneGroup)
	local bg, cloud, staticCloud1, staticCloud2
	bg = display.newImage( "images/sea1.jpg", display.contentCenterX, display.contentCenterY + 150 );
	cloud = display.newImage( "images/cloud.png", display.contentCenterX - 150, display.contentCenterY);
	staticCloud1 = display.newImage( "images/cloud.png", display.contentCenterX - 150, display.contentCenterY - 250 );
	staticCloud2 = display.newImage( "images/cloud.png", display.contentCenterX + 250, display.contentCenterY - 150);
	staticCloud1.alpha = 0.5
	staticCloud2.alpha = 0.7
	staticCloud2.xScale = 0.5
	staticCloud2.yScale = 0.5
	sceneGroup:insert(bg)
	sceneGroup:insert(cloud)
	sceneGroup:insert(staticCloud1)
	sceneGroup:insert(staticCloud2)
	Runtime:addEventListener("enterFrame", function ( event )
		bg.x = bg.x - 0.5
		cloud.x = cloud.x - 0.7
		if bg.x <= 335 then
			bg.x = display.contentCenterX
		end
		if cloud.x <= -100 - cloud.contentWidth/2 then
			cloud.x = display.contentWidth + cloud.contentWidth/2 + 100
			cloud.xScale = math.random(20, 100) / 100
		end
	end)
end

function scene:create( event )
	local sceneGroup = self.view
	createBackground(sceneGroup)
end

local function gotoNextLevel()
	composer.gotoScene("level2",{params = {score=level1.gameState.score}})
end 

--function to reset the game variables to initial values
function resetVariables()
	level1.enemyCount = const.level1.enemyCount
	level1.enemies = table.deepcopy(level1._enemies)
	level1.enemyTypes = const.level1.enemyTypes
	level1.enemyInterval = const.level1.enemyInterval
	level1.gameState = {score = 0, enemyDestroyed = 0, playerHealth = const.level1.playerHealth, }
				
end 

--function to check if the game is finished
local function checkGameStatus( event )
	--if all enemies are finished, player advances to next level
	if level1.gameState.enemyDestroyed == level1.enemyCount then
		Runtime:removeEventListener( "enterFrame", checkGameStatus )
		level1:removeListeners()
		timer.performWithDelay( 1000, function(event)
			local nextLevel = display.newImage("images/btnNext.png", display.contentCenterX, display.contentCenterY - 50)
			sceneGroup:insert(nextLevel)
			
			local prevLevel = display.newImage("images/btnPlayAgain.png", display.contentCenterX, display.contentCenterY)
			sceneGroup:insert(prevLevel)

			local quitGame = display.newImage("images/btnQuit.png", display.contentCenterX, display.contentCenterY + 50)
			sceneGroup:insert(quitGame)

			nextLevel:addEventListener( "tap", function (event) 
				nextLevel:removeSelf( )
				prevLevel:removeSelf( )
				quitGame:removeSelf( )
				gotoNextLevel()
			end)

			prevLevel:addEventListener( "tap", function ( event )
				nextLevel:removeSelf( )
				prevLevel:removeSelf( )
				quitGame:removeSelf( )

				level1:clean()
				resetVariables()
			
				level1:initializeEnemy()
				level1:initializePlayer()
				level1:updateHealth()
				level1:updateScore()

				-- Add event listener for overlay
				level1:addListeners()
				Runtime:addEventListener( "enterFrame", checkGameStatus )				
			end)

			quitGame:addEventListener( "tap", function (event)
				composer.gotoScene("end", {params={level=level1.name,score=level1.gameState.score}})
			end)	
		end)
	--if player has no remaining health, destroy the player 
	elseif (level1.gameState.playerHealth == 0) then
		Runtime:removeEventListener( "enterFrame", checkGameStatus )
		level1:explodePlayer(event)
		level1:cleanAllEnemies()
		resetVariables()
		timer.performWithDelay( 1000, function(event)
			-- level1.startPlay = display.newImage("images/btnPlayAgain.png", display.contentCenterX, display.contentCenterY)
			-- level1:resetPlay()

			local prevLevel = display.newImage("images/btnPlayAgain.png", display.contentCenterX, display.contentCenterY)
			sceneGroup:insert(prevLevel)
			local quitGame = display.newImage("images/btnQuit.png", display.contentCenterX, display.contentCenterY + 50)
			sceneGroup:insert(quitGame)

			prevLevel:addEventListener( "tap", function ( event )
				prevLevel:removeSelf( )
				quitGame:removeSelf( )

				level1:clean()
				resetVariables()
			
				level1:initializeEnemy()
				level1:initializePlayer()
				level1:updateHealth()
				level1:updateScore()

				-- Add event listener for overlay
				level1:addListeners()
				Runtime:addEventListener( "enterFrame", checkGameStatus )				
			end)


			quitGame:addEventListener( "tap", function (event)
				level1.startPlay.isVisible = false
				composer.gotoScene("end", {params={level=level1.name,score=level1.gameState.score}})
			end)

			Runtime:addEventListener( "enterFrame", checkGameStatus )
		end)
	end
end

function scene:show( event )
	sceneGroup = self.view
	if (event.phase == "did") then
		level1 = stage:new({
				name = const.level1.name,
				enemyCount = const.level1.enemyCount,
				enemies = const.level1.enemies,
				enemyTypes = const.level1.enemyTypes,
				enemyInterval = const.level1.enemyInterval,
				sceneGroup = sceneGroup,
				gameState = {score = 0, enemyDestroyed = 0, playerHealth = const.level1.playerHealth, },
			})
		level1:create()
		Runtime:addEventListener( "enterFrame", checkGameStatus )
	end
end

function scene:hide(event)
	if(event.phase == "will") then
		if(level1.player ~= nil) then
			level1.player:clean()
		end
		level1:cleanAllEnemies()
	end 
end 

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )

return scene