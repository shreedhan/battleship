local bullet = require("bullet")
local collisionFilters = require("collisionFilters")
local audioTable = require("audioTable")

local enemyBullet = bullet:new( {
		name="enemyBullet", 
		collisionFilter=collisionFilters.enemyBullet,
	} )


return enemyBullet