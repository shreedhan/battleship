-- enemy.lua
-- This file is base class for all of the enemies

local sheet = require("sheet")
local const = require("const")
local bullet = require("enemyBullet")
local explosion = require("explosion")
local collisionFilters = require("collisionFilters")

-- Initial parameters for the enemy object
local enemy = {
	-- Initialize enemy from constant values
	tag 		= const.enemy.tag,
	name 		= const.enemy.name,
	health 		= const.enemy.health,
	entrySound 	= const.enemy.entrySound,
	fireRate	= const.enemy.fireRate,
	fireSound 	= const.enemy.fireSound,
	hurtSound	= const.enemy.hurtSound,
	seqData 	= const.enemy.seqData,
	xPos 		= const.enemy.xPos,
	yPos 		= const.enemy.yPos,
	speed 		= const.enemy.speed,
	xStopAt 	= const.enemy.xStopAt,
	leaveAfter 	= const.enemy.leaveAfter,
	xDistance 	= const.enemy.xDistance,
	density		= const.enemy.density,

	
	-- Constructor for enemy
	new = function ( self, o )
		o = o or {}
		setmetatable( o, self )
		self.__index = self
		return o
	end,
	
	-- Spawn function for enemy
	-- Create sprite for enemy, position enemy
	spawn = function ( self )
		local sheet 	= sheet:createImageSheet()
		local enemy 	= display.newSprite( sheet, self.seqData )
		enemy:addEventListener( "collision", self)
		enemy.x 		= self.xPos
		enemy.y 		= self.yPos
		self.dispObj 	= enemy
		enemy.pp 		= self
		self.sceneGroup:insert(enemy)
		local enemyOutline = graphics.newOutline(2, sheet, self.outlineFrameIndex)
		self.loadedEntrySound = audio.loadSound( self.entrySound )
		self.physics.addBody(enemy, "dynamic", {density = self.density, filter = collisionFilters.enemy, outline=enemyOutline})
	end,

	-- Enemy enters the scene
	-- Start playing the enmey's animation sequence and start fire
	-- Start sound for the enemy and start transition
	-- On complete of this transition, the enemy stays at its stop point
	enter = function ( self )
		self.dispObj:play()
		self:startFire()
		self.entrySoundChannel = audio.play( self.loadedEntrySound, {loops = -1} )
		self.enterTransitionID = transition.to( self.dispObj, {x = self.xStopAt, time = (self.dispObj.x - self.xStopAt) / self.speed, onComplete=function(event) self:stay() end} )
	end,

	-- Enemy stops firing and stays at its stay point for 2000ms (TODO: maybe needs to be dynamic) and exits
	stay = function ( self, event )
		self.exitTimerID = timer.performWithDelay( 2000, function ( event )
			self:exit()
		end, 1 )
	end,

	-- Enemy exits from the scene
	-- Remove the enemy display object and the sound channel
	exit = function ( self )
		self:endFire()
		self.exitTransitionID = transition.to( self.dispObj, {x = -100, time = (self.dispObj.x + 100) / self.speed, onComplete=function(event)
				self.dispObj:removeSelf( )
				self.dispObj = nil
				-- audio.fadeOut( self.entrySoundChannel )
				self:clean()
			end
		} )
	end,

	-- Explode enemy
	explode = function ( self )
		self:endFire()
		audio.stop(self.entrySoundChannel)
		local enemyExplosion = explosion:new({xPos = self.dispObj.x, yPos = self.dispObj.y})
		self.dispObj:removeSelf( )
		enemyExplosion:spawn()
		self:clean()
	end,

	-- Start fire
	-- Starts a timer to repeatedly fire
	-- It has 50% chance of firing or not firing (TODO: needs to be dynamic later)
	-- Other subclasses MUST store the fire timer's ID in self.fireTimerID
	startFire = function (self)
		self.fireTimerID = timer.performWithDelay( 500, function ( event )
				if (math.random() < 0.5) then
					self:fire()
			end
		end, -1 )
	end,

	-- End fire, cancels the timer to fire repeatedly
	endFire = function ( self )
		if self.fireTimerID ~= null then
			timer.cancel( self.fireTimerID )
		end
	end,

	-- Fire function, creates bullet, spawns
	fire = function ( self )
		if (self.physics == null) then
			print("Physics not enabled. Cannot fire")
			return
		end
		local bulletObj = bullet:new({
				physics = self.physics,
				xForce = -0.05,
				yForce = 0.05,
				xPos = self.dispObj.x,
				yPos = self.dispObj.y,
				sound = self.fireSound,
				rotation = 45,
			})
		bulletObj:spawn()
	end,

	-- Collision handler for enemy
	collision = function (self, event)
		-- print (self.name, "collided with", event.other.pp.name, "when health is", self.health)
		if (event.other.pp.name == "playerBullet") then
			self.health = self.health - 1
			local enemyHurtSound = audio.loadSound( self.hurtSound )
			audio.play( enemyHurtSound )
			self.stage.gameState.score = self.stage.gameState.score + 100
			self.stage:updateScore()
		end
		if (self.health <= 0) then
			self:explode()
		end
	end,

	-- Clean function to remove the object, audio, related timers etc. from the game
	clean = function ( self )
		
		if self.fireTimerID ~= nil then
			timer.cancel( self.fireTimerID )
		end
		if self.exitTimerID ~= nil then
			timer.cancel( self.exitTimerID )
		end
		if self.exitTransitionID ~= nil then
			transition.cancel( self.exitTransitionID)
		end
		if self.enterTransitionID ~= nil then
			transition.cancel( self.enterTransitionID)
		end
		if self.dispObj ~= nil then
			self.dispObj:removeSelf( )
			self.dispObj = nil
		end
		audio.stop(self.entrySoundChannel)
		self.stage.gameState.enemyDestroyed = self.stage.gameState.enemyDestroyed + 1
		self.stage.activeEnemies[self] = nil
		self = nil
	end
}

return enemy