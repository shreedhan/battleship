-- main.lua
-- This is the entry point for game
local composer = require("composer");

-- Activate multitouch support
system.activate( "multitouch" )

-- Goto scene
composer.gotoScene("start");