--represents level 3 of the game
local composer = require("composer")
local stage = require("stage")
local const = require("const")
local deepcopy = require("deepcopy")

local scene = composer.newScene()
local level3
local sceneGroup

function createBackground(sceneGroup)
	local bg, cloud, staticCloud1, staticCloud2
	bg = display.newImage( "images/sea3.jpg", display.contentCenterX, display.contentCenterY);
	cloud = display.newImage( "images/cloud.png", display.contentCenterX - 150, display.contentCenterY);
	staticCloud1 = display.newImage( "images/cloud.png", display.contentCenterX - 150, display.contentCenterY - 250 );
	staticCloud2 = display.newImage( "images/cloud.png", display.contentCenterX + 250, display.contentCenterY - 150);
	cloud.alpha = 0.5
	staticCloud1.alpha = 0.5
	staticCloud2.alpha = 0.7
	staticCloud2.xScale = 0.5
	staticCloud2.yScale = 0.5
	sceneGroup:insert(bg)
	sceneGroup:insert(cloud)
	sceneGroup:insert(staticCloud1)
	sceneGroup:insert(staticCloud2)
	Runtime:addEventListener("enterFrame", function ( event )
		cloud.x = cloud.x - 0.7
		if cloud.x <= -100 - cloud.contentWidth/2 then
			cloud.x = display.contentWidth + cloud.contentWidth/2 + 100
			cloud.xScale = math.random(20, 100) / 100
		end
	end)
end

function scene:create( event )
	local sceneGroup = self.view
	createBackground(sceneGroup)
end

local function gotoNextLevel()
	composer.gotoScene("level4",{params = {score=level3.gameState.score}})
end 

--function to reset the game variables to initial values
function resetVariables()
	level3.enemyCount = const.level3.enemyCount
	level3.enemies = table.deepcopy(level3._enemies)
	level3.enemyTypes = const.level3.enemyTypes
	level3.enemyInterval = const.level3.enemyInterval
	level3.gameState = {score = level3.pScore, enemyDestroyed = 0, playerHealth = const.level3.playerHealth, }
				
end 

--function to check if the game is finished
local function checkGameStatus( event )
	--if all enemies are finished, player advances to next level
	if level3.gameState.enemyDestroyed == level3.enemyCount then
		Runtime:removeEventListener( "enterFrame", checkGameStatus )
		level3:removeListeners()
		timer.performWithDelay( 1000, function(event)
			local prevLevel = display.newImage("images/btnPlayAgain.png", display.contentCenterX, display.contentCenterY)
			sceneGroup:insert(prevLevel)

			local quitGame = display.newImage("images/btnQuit.png", display.contentCenterX, display.contentCenterY + 40)
			sceneGroup:insert(quitGame)

			prevLevel:addEventListener( "tap", function ( event )
				prevLevel:removeSelf( )
				quitGame:removeSelf( )
				level3:clean()
				resetVariables()
			
				level3:initializeEnemy()
				level3:initializePlayer()
				level3:updateHealth()
				level3:updateScore()

				-- Add event listener for overlay
				level3:addListeners()
				Runtime:addEventListener( "enterFrame", checkGameStatus )				
			end )

			quitGame:addEventListener( "tap", function (event)
				composer.gotoScene("end", {params={level=level3.name,score=level3.gameState.score}})
			end)

		end)
	--if player has no remaining health, destroy the player 
	elseif (level3.gameState.playerHealth == 0) then
		Runtime:removeEventListener( "enterFrame", checkGameStatus )
		level3:explodePlayer(event)
		level3:cleanAllEnemies()
		resetVariables()
		timer.performWithDelay( 1000, function(event)
			level3.startPlay = display.newImage("images/btnPlayAgain.png", display.contentCenterX, display.contentCenterY)
			level3:resetPlay()

			local quitGame = display.newImage("images/btnQuit.png", display.contentCenterX, display.contentCenterY + 50)
			sceneGroup:insert(quitGame)

			quitGame:addEventListener( "tap", function (event)
				level3.startPlay.isVisible = false
				quitGame:removeSelf( )
				composer.gotoScene("end", {params={level=level3.name,score=level3.gameState.score}})
			end)

			Runtime:addEventListener( "enterFrame", checkGameStatus )
		end)
	end
end

function scene:show( event )
	sceneGroup = self.view
	if (event.phase == "did") then
		level3 = stage:new({
				name = const.level3.name,
				enemyCount = const.level3.enemyCount,
				enemies = const.level3.enemies,
				enemyTypes = const.level3.enemyTypes,
				enemyInterval = const.level3.enemyInterval,
				sceneGroup = sceneGroup,
				gameState = {score = event.params.score, enemyDestroyed = 0, playerHealth = const.level3.playerHealth,},
			})
		level3.pScore = event.params.score
		level3:create()
		Runtime:addEventListener( "enterFrame", checkGameStatus )

	end
end

function scene:hide(event)
	if(event.phase == "will") then
		if(level3.player ~= nil) then
			level3.player:clean()
		end 
	end 
end 

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )

return scene