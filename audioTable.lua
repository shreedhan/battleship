-- Audio table for entire game
local audioTable = {
	explosion 			= "audio/explosion.wav",
	enemy1Enter			= "audio/enemy1Enter.wav",
	enemy1Fire			= "audio/enemy1Fire.wav",
	enemy2Enter			= "audio/enemy2Enter.wav",
	enemy2Fire			= "audio/enemy2Fire.wav",
	enemy3Enter			= "audio/enemy3Enter.wav",
	enemy3Fire			= "audio/enemy3Fire.wav",
	enemy4Enter			= "audio/enemy3Enter.wav",
	enemy4Fire			= "audio/enemy3Fire.wav",
	enemy5Enter			= "audio/enemy5Enter.wav",
	enemy5Fire			= "audio/enemy5Fire.wav",
	bossEnter			= "audio/bossEnter.wav",
	bossFire			= "audio/bossFire.wav",
	helicopterEnter		= "audio/helicopterEnter.wav",
	helicopterFire 		= "audio/helicopterFire.wav",
	playerBulletFire1 	= "audio/playerBulletFire1.wav",
	enemyHurt 			= "audio/enemyHurt.wav",
}

return audioTable