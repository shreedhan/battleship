--class for player object
local sheet = require("sheet")
local gun = require("gun")
local bullet = require("playerBullet")
local const = require("const")
local explosion = require("explosion")
local collisionFilters = require("collisionFilters")

local player = {
	name = const.player.name,
	tag = const.player.tag,
	xPos = const.player.xPos,
	yPos = const.player.yPos,
	health = const.player.health,
	hurtSound = const.player.hurtSound,
	outlineFrameIndex = const.player.outlineFrameIndex,
	seqData = {
			{ name="normal", frames={1}, time=10, loopCount=1 },
		},
}

function player:new(o)
	o = o or {}
	setmetatable( o, self )
	self.__index = self
	return o
end 

--function to create player instance consisting of ship and gun
function player:spawn()
	local shipSheet = sheet:createImageSheet()
	local ship = display.newSprite( shipSheet, self.seqData )

	ship.x = self.xPos
	ship.y = self.yPos

	local gun = gun:new({rotation = 45, physics = self.physics})
	gun = gun:spawn()

	self.gun = gun
	self.dispObj = ship
	self.dispObj.pp = self

	local playerOutline = graphics.newOutline(2, shipSheet, self.outlineFrameIndex)
	self.physics.addBody(self.dispObj, "static", {outline = playerOutline, filter = collisionFilters.player})
	self.sceneGroup:insert(self.dispObj)
	self.sceneGroup:insert(gun.dispObj)
	self.dispObj:addEventListener( "collision", self ) --add collision event listener to player
	return self
end 

--function to handle the firing of bullets by player
function player:fire(event) 
	local phase = event.phase
	local rotation = self.gun.rotation
	local len = self.gun.dispObj.height
	local angleInRadian = rotation*math.pi/180 --convert to radian unit

	local bulletData = {
		xPos = self.gun.dispObj.x + (len * math.sin(angleInRadian)),
		yPos = self.gun.dispObj.y - (len * math.cos(angleInRadian)),
		physics = self.physics,
		xForce = 0.1*math.sin(angleInRadian),
		yForce = - 0.1*math.cos(angleInRadian),
		rotation = self.gun.rotation,
	}
	
	local playerBullet = bullet:new(bulletData)
	playerBullet = playerBullet:spawn()
	playerBullet.dispObj.anchorX = 0.5; playerBullet.dispObj.anchorY = 1
	playerBullet.dispObj:setFillColor( 1,0,0 )
	
end 

--function to handle the collision of player
function player:collision(event)
	local playerHurtSound = audio.loadSound( self.hurtSound )
	audio.play( playerHurtSound )

	self.gameState.playerHealth = self.gameState.playerHealth - 1
	print("Health: ", self.gameState.playerHealth)
	self.pp:updateHealth()
end 

--function to display explosion sequence when player's health reduces to zero
function player:explode(event)
	self.dispObj:removeEventListener( "collision", self )

	local playerExplosion1 = explosion:new({xPos = self.dispObj.x, yPos = self.gun.dispObj.y})
	local playerExplosion2 = explosion:new({xPos = self.dispObj.x - 60, yPos = self.gun.dispObj.y})
	local playerExplosion3 = explosion:new({xPos = self.dispObj.x + 50, yPos = self.gun.dispObj.y})
	playerExplosion1:spawn()
	playerExplosion2:spawn()
	playerExplosion3:spawn()
	
	self:clean()
end 

--function to clean the objects associated with player 
function player:clean()
	if (self ~= nil) then
		if (self.gun ~= nil) then
			if (self.gun.dispObj ~= nil) then 
				self.gun.dispObj:removeSelf()
				self.gun.dispObj = nil
			end 
			self.gun = nil
		end 
		if (self.dispObj ~= nil) then
			self.dispObj:removeSelf()
			self.dispObj = nil
		end 
		self = nil
	end 
end 

return player