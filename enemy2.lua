-- Enemy - Boss
local enemy = require("enemy")
local audioTable = require("audioTable")

local enemy2 = {
	name = "enemy2",
	health = 4,
	entrySound = audioTable.enemy2Enter,
	fireSound = audioTable.enemy2Fire,
	speed = 0.1,
	seqData = {{ name="normal", frames={4} } },
	outlineFrameIndex = 4,
	fireRate = 2,
}

enemy2 = enemy:new(enemy2)

return enemy2