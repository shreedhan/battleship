local collisionFilters = {
	player = { categoryBits = 1, maskBits = 8},
	enemy = { categoryBits = 2, maskBits = 4},
	playerBullet = { categoryBits = 4, maskBits = 58},
	enemyBullet = { categoryBits = 8, maskBits = 197},
	wallTop = { categoryBits = 16, maskBits = 4},
	wallRight = { categoryBits = 32, maskBits = 4},
	wallBottom = { categoryBits = 64, maskBits = 8},
	wallLeft = { categoryBits = 128, maskBits = 8},
}

return collisionFilters;