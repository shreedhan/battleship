-- Enemy - Boss
local enemy = require("enemy")
local audioTable = require("audioTable")

local boss = {
	name = "boss",
	health = 15,
	entrySound = audioTable.bossEnter,
	fireSound = audioTable.bossFire,
	speed = 0.02,
	seqData = {{ name="normal", frames={6} } },
	outlineFrameIndex = 6,
	fireRate = 2,
}

boss = enemy:new(boss)

return boss