-- Require audio table
local audioTable = require("audioTable")

-- Define constants
local const = {
	-- Default properties for enemy
	enemy = {
		tag = "enemy",
		name = "default",
		health = 10,
		entrySound = nil,
		fireSound = nil,
		hurtSound = audioTable.enemyHurt,
		seqData = nil,
		xPos = display.contentWidth,
		yPos = 150,
		speed = 0.05,
		xStopAt = 300,
		leaveAfter = 10000,
		fireRate = 0.5,
		density = 100,
	},

	-- Default values for bullet
	bullet = {
		tag = "bullet",
		xForce = 0.1,
		yForce = 0.1,
	},

	-- Default values for gun
	gun = {
		tag = "gun",
		name = "gun",
		xPos = 150,
		yPos = display.contentHeight - 60,
		seqData = nil,
		rotation = 45,
	},

	-- Default values for player
	player = {
		tag = "player",
		name = "player",
		xPos = 150,
		yPos = display.contentHeight - 100,
		seqData = nil,
		health = 10,
		hurtSound = audioTable.enemyHurt,
		outlineFrameIndex = 1,
	},

	-- Level 1 properties
	level1 = {
		name = "Level1",
		enemyCount = 5,
		enemyTypes = 1,
		enemies = {
			{type = "enemy2", count = 5},
		},
		enemyInterval = 5000,
		playerHealth = 10,
	},

	-- Level 2 properties
	level2 = {
		name = "Level2",
		enemyCount = 10,
		enemyTypes = 4,
		enemies = {
			{type = "enemy1", count = 2},
			{type = "helicopter", count = 3},
			{type = "enemy3", count = 3},
			{type = "enemy2", count = 2},
		},
		enemyInterval = 4500,
		playerHealth = 15,
	},

	-- Level 3 properties
	level3 = {
		name = "Level3",
		enemyCount = 15,
		enemyTypes = 5,
		enemies = {
			{type = "enemy1", count = 3},
			{type = "enemy2", count = 3},
			{type = "enemy3", count = 3},
			{type = "boss", count = 3},
			{type = "helicopter", count = 3},
		},
		enemyInterval = 4500,
		playerHealth = 15,
	},
};

return const;