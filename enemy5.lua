-- Enemy - Boss
local enemy = require("enemy")
local audioTable = require("audioTable")

local enemy5 = {
	name = "enemy5",
	health = 8,
	entrySound = audioTable.enemy5Enter,
	fireSound = audioTable.enemy5Fire,
	speed = 0.05,
	seqData = {{ name="normal", frames={7} } },
	outlineFrameIndex = 7,
	fireRate = 2,
}

enemy5 = enemy:new(enemy5)

return enemy5