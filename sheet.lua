local sheet = {
	sheetOpt = {
		frames = {
			{ x=40,		y=9,	width=304,	height=153},	-- 1. ship
			{ x=33,		y=172,	width=15,	height=75},		-- 2. gun
			{ x=237,	y=184,	width=105,	height=31},		-- 3. enemy1
			{ x=233,	y=232,	width=111,	height=35},		-- 4. enemy2
			{ x=59,		y=233,	width=158,	height=52},		-- 5. enemy3
			{ x=54,		y=304,	width=269,	height=59},		-- 6. enemy4 -- Boss
			{ x=57,		y=392,	width=138,	height=79},		-- 7. enemy5
			{ x=62,		y=491,	width=71,	height=48},		-- 8. enemy61 -- helicopter
			{ x=133,	y=491,	width=71,	height=48},		-- 9. enemy62 -- helicopter
			{ x=200,	y=491,	width=71,	height=48},		-- 10. enemy63 -- helicopter
			{ x=272,	y=491,	width=71,	height=48},		-- 11. enemy64 -- helicopter
			{ x=401,	y=23,	width=52,	height=53},		-- 12. explosion1
			{ x=463,	y=19,	width=56,	height=60},		-- 13. explosion2
			{ x=526,	y=17,	width=58,	height=63},		-- 14. explosion3
			{ x=590,	y=16,	width=58,	height=64},		-- 15. explosion4
			{ x=398,	y=80,	width=58,	height=63},		-- 16. explosion5
			{ x=463,	y=81,	width=56,	height=62},		-- 17. explosion6
			{ x=527,	y=79,	width=55,	height=63},		-- 18. explosion7
			{ x=592,	y=82,	width=54,	height=59},		-- 19. explosion8
			{ x=401,	y=146,	width=52,	height=58},		-- 20. explosion9
			{ x=466,	y=147,	width=50,	height=67},		-- 21. explosion10
			{ x=531,	y=148,	width=49,	height=55},		-- 22. explosion11
			{ x=595,	y=148,	width=48,	height=54},		-- 23. explosion12
			{ x=404,	y=213,	width=47,	height=53},		-- 24. explosion13
			{ x=469,	y=213,	width=45,	height=52},		-- 25. explosion14
			{ x=533,	y=214,	width=44,	height=50},		-- 26. explosion15
			{ x=599,	y=214,	width=39,	height=44},		-- 27. explosion16
		},
	},

	createImageSheet = function ( self )
		if (self.sheet ~= nil) then
			return self.sheet;
		end
		local sheet = graphics.newImageSheet( "images/battleship.png", self.sheetOpt );
		self.sheet = sheet;
		return self.sheet;
	end,
}

return sheet