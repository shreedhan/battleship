--class for bullet object
local const = require( "const")

local bullet = {
	name = "bullet",
	xPos = 100,
	yPos = 100,
	height=10,
	width=3,
	xForce = const.bullet.xForce,
	yForce = const.bullet.yForce,
}

function bullet:new(o)
	o = o or {}
	setmetatable( o, self )
	self.__index = self
	return o
end 

--function to create bullet at given position and add to physics engine
function bullet:spawn()
	local bullet = display.newRect( self.xPos, self.yPos, self.width, self.height )
	self.dispObj = bullet
	self.dispObj.pp = self
	if (self.rotation ~= nil) then
		self.dispObj.rotation = self.rotation
	end
	if (self.physics == nil) then
		print("Physics not enabled in bullet spawn")
		return self
	else
		self.physics.addBody(self.dispObj, "dynamic", {filter=self.collisionFilter})
		self:applyForce()
		local sound = audio.loadSound( self.sound )
		audio.play( sound )
	end
	self.dispObj:addEventListener( "collision", self )
	return self
end 

--function to handle the collision of bullet with other objects
function bullet:collision(event)
	-- print(self.name, "Collided with ", event.other.pp.name)
	self.dispObj:removeSelf( )
end 

function bullet:applyForce()
	self.dispObj:applyForce( self.xForce, self.yForce, self.dispObj.x, self.dispObj.y )
end 

return bullet