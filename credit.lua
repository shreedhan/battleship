local composer = require("composer")

local scene = composer.newScene()

function scene:create(event)
end

function scene:show( event )
	local sceneGroup = self.view
	local bg = display.newImage( "images/sea1.jpg", display.contentCenterX, display.contentCenterY + 50 )
	sceneGroup:insert(bg)

	local backBtn = display.newImage( "images/btnBack.png", display.contentWidth - 100, 50 )
	--local backBtn = display.newText( { text = "<< Back", fontSize = 30, x = display.contentWidth - 100, y = 50} )
	backBtn:addEventListener( "tap", function ( event )
		composer.gotoScene("start")
	end )
	sceneGroup:insert( backBtn )

	local creditString = "Images were used from: \n" .. 
		"Sprite Database http://spritedatabase.net/file/5244\n" .. 
		"Retro gamezone http://www.retrogamezone.co.uk/unsquadron.htm\n" .. 
		"Pixbay http://pixabay.com/en/sea-ocean-infinity-wide-loneliness-186419/\n" ..
		"\n\nTools used: \n" ..
		"Text craft http://www.textcraft.net\n" ..
		"GIMP http://www.gimp.org\n" ..
		"Bfxr.net http://bfxr.net\n" ..
		"Github Gist Lua nonrecursive Deepcopy https://gist.github.com/Deco/3985043\n"
	local creditText = display.newText({text = creditString, fontSize = 25, x = display.contentCenterX, y = display.contentCenterY})
	transition.to( creditText, {time = 5000, y = -creditText.contentHeight / 2, onComplete = function ( event )
		local createdBy = display.newImage( "images/created.png", display.contentCenterX, display.contentCenterY - 50)
		-- local createdBy = display.newText( { text = "Created By:\nShreedhan Shrestha\nSuman Shakya", fontSize = 50, x = display.contentCenterX, y = display.contentCenterY} )
		sceneGroup:insert(createdBy)
	end} )
	sceneGroup:insert( creditText )
end

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )

return scene