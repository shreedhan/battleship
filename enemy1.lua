-- Enemy - Boss
local enemy = require("enemy")
local audioTable = require("audioTable")

local enemy1 = {
	name = "enemy1",
	health = 2,
	entrySound = audioTable.enemy1Enter,
	fireSound = audioTable.enemy1Fire,
	speed = 0.05,
	seqData = {{ name="normal", frames={3} } },
	outlineFrameIndex = 3,
	fireRate = 2,
}

enemy1 = enemy:new(enemy1)

return enemy1