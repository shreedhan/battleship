local composer = require("composer")
local scene = composer.newScene()
local sceneGroup

local function playGame()
	composer.gotoScene("level1")
end

local function showCredit()
	composer.gotoScene("credit")
end 

function scene:create(event)
	sceneGroup = self.view
	local bg = display.newImage( "images/sea1.jpg", display.contentCenterX, display.contentCenterY + 50 );

	-- local bg = display.newRect(display.contentCenterX, display.contentCenterY, display.contentWidth, display.contentHeight)
	-- bg:setFillColor( 102/255,178/255,1)
	sceneGroup:insert(bg)
	
	local battleShipeImage = display.newImage( "images/name.png",display.contentCenterX,display.contentCenterY - 150 )
	sceneGroup:insert(battleShipeImage)

	local playGameBtn = display.newImage( "images/btnPlay.png",display.contentCenterX,display.contentCenterY - 50 )
	sceneGroup:insert(playGameBtn)

	local showCreditBtn = display.newImage( "images/btnCredit.png",display.contentCenterX,display.contentCenterY + 10 )
	sceneGroup:insert(showCreditBtn)

	playGameBtn:addEventListener( "tap", playGame )
	showCreditBtn:addEventListener( "tap", showCredit )

end

function scene:show(event)

end 

scene:addEventListener( "create", scene )
return scene