--class for gun object
local sheet = require("sheet")
local const = require("const")
local bullet = require("bullet")

local gun = {
	name = const.gun.name,
	tag = const.gun.tag,
	xPos = const.gun.xPos,
	yPos = const.gun.yPos,
	rotation = const.gun.rotation,
	seqData = {
		{name="normal", frames={2}, time=10, loopCount=1},
	},
}

function gun:new(o)
	o = o or {}
	setmetatable( o, self )
	self.__index = self
	return o
end 

--function to create gun object from the imagesheet
function gun:spawn()
	local sheet = sheet:createImageSheet()
	local gun = display.newSprite( sheet, self.seqData )
	gun.anchorx = 0.5; gun.anchorY = 1

	gun.x = self.xPos
	gun.y = self.yPos
	self.dispObj = gun

	self:move()
	return self
end 

--function to handle the movement of gun
function gun:move(event)
	if(self.rotation > 75) then
			self.rotation = 75
	elseif (self.rotation < 0) then
		self.rotation = 0
	end 
	self.dispObj:rotate(self.rotation)
end 

return gun