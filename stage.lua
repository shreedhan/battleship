--represents the framework for the game consisting of walls, player and enemies
local collisionFilters = require( "collisionFilters" )
local helicopter = require("helicopter")
local boss = require("boss")
local enemy1 = require("enemy1")
local enemy2 = require("enemy2")
local enemy3 = require("enemy3")
local enemy4 = require("enemy4")
local enemy5 = require("enemy5")
local player = require("player");
local physics = require("physics")
local const = require("const")
local explosion = require("explosion")
local deepcopy = require("deepcopy")

physics.start( )
-- physics.setDrawMode( "hybrid" )
physics.setGravity( 0, 0 )

local enemyYPosFlag = true

local stage = {
	name = "stage",
	gameState = {},
	activeEnemies = {},
}

function stage:new( o )
	o = o or {}
	setmetatable( o, self )
	self.__index = self
	self._enemies = table.deepcopy(o.enemies)
	return o
end

--creates wall and add them to physics engine; create start button for the player to start playing current level
function stage:initializeStage(  )
	-- Create walls and overlay and fill all with red color
	self.left = display.newRect( 0, 0, 1, display.contentHeight )
	self.top = display.newRect( 0, 0, display.contentWidth, 1 )
	self.right = display.newRect( display.contentWidth - 1, 0, 1, display.contentHeight )
	self.bottom = display.newRect( 0, display.contentHeight - 1, display.contentWidth, 1 )
	self.overlay = display.newRect( 0, 0, display.contentWidth, display.contentHeight )
	self.startPlay = display.newImage("images/btnStart.png", display.contentCenterX, display.contentCenterY)
	self.gameOver = display.newText({text="Game Over", fontSize=40, x = display.contentCenterX, y = display.contentCenterY, font = native.systemFontBold })

	--self.healthText = display.newText( { text = "Health: " .. self.gameState.playerHealth, x = 100, y = 20, fontSize = 20} )
	
	self:initializeScoreBar()
	local level = "images/"..self.name..".png"
	self.levelText = display.newImage( level, display.contentCenterX, 30)
	self.scoreText = display.newText( { text = "Score: " .. self.gameState.score, x = display.contentWidth - 170, y = 15, fontSize = 25} )
	self.scoreText.anchorX = 0
	self.scoreText.anchorY = 0
	self.left:setFillColor( 1, 0, 0 )
	self.top:setFillColor( 1, 0, 0 )
	self.right:setFillColor( 1, 0, 0 )
	self.bottom:setFillColor( 1, 0, 0 )
	self.overlay:setFillColor( 1, 0, 0)
	self.overlay.alpha = 0.01

	-- Give name to all objects
	self.left.pp = self
	self.top.pp = self
	self.right.pp = self
	self.bottom.pp = self
	self.overlay.pp = self

	-- Anchor all objects
	self.left.anchorX = 0
	self.left.anchorY = 0
	self.top.anchorX = 0
	self.top.anchorY = 0
	self.right.anchorX = 0
	self.right.anchorY = 0
	self.bottom.anchorX = 0
	self.bottom.anchorY = 0
	self.overlay.anchorX = 0
	self.overlay.anchorY = 0

	self.sceneGroup:insert(self.left)
	self.sceneGroup:insert(self.top)
	self.sceneGroup:insert(self.right)
	self.sceneGroup:insert(self.bottom)
	self.sceneGroup:insert(self.overlay)
	self.sceneGroup:insert(self.startPlay)
	self.sceneGroup:insert(self.gameOver)
	--self.sceneGroup:insert(self.healthText)

	self.sceneGroup:insert(self.levelText)
	self.sceneGroup:insert(self.scoreText)
	
	self.gameOver.isVisible = false

	-- Add objects statically
	physics.addBody( self.left, "static", {filter=collisionFilters.wallLeft} )
	physics.addBody( self.top, "static", {filter=collisionFilters.wallTop} )
	physics.addBody( self.right, "static", {filter=collisionFilters.wallRight} )
	physics.addBody( self.bottom, "static", {filter=collisionFilters.wallBottom} )


	self.startPlay:addEventListener( "tap", function ( event )
			self.startPlay:removeSelf( )
			self.startPlay = nil
			self:initializeEnemy()
			self:initializePlayer()
			
			-- Add event listener for overlay
			self.overlay:addEventListener( "touch", self)
			self.overlay:addEventListener( "tap", self)
			
	end )
end

--function to add event listeners for tap and touch on the screen
function stage:addListeners()
	-- Add event listener for overlay
	self.overlay:addEventListener( "touch", self)
	self.overlay:addEventListener( "tap", self)
end 

--function to reinitialize game components: player and enemies
function stage:resetPlay()
	self.startPlay:addEventListener( "tap", function ( event )
		self.startPlay:removeSelf( )
		self.startPlay = nil
		-- self:cleanAllEnemies()
		self:initializeEnemy()
		self:initializePlayer()
		self:updateHealth()
		self:updateScore()
		self:addListeners()
	end )	
end 

--funciton to update the score text each time player hits enemy
function stage:updateScore( )
	self.scoreText.text = "Score: " .. self.gameState.score
end

--function to initialize the health bar to be shown on the top left side of the screen
function stage:initializeScoreBar()
	self.totalHealth = self.gameState.playerHealth
	self.outerBar = display.newRect( 10, 10, 100, 20 )
	self.outerBar.anchorX = 0
	self.outerBar.anchorY = 0
	self.sceneGroup:insert( self.outerBar)

	self.innerBar = display.newRect(12, 12, 96, 16)
	self.innerBar.anchorX = 0
	self.innerBar.anchorY = 0
	self.innerBar:setFillColor( 0, 1, 0 )
	self.sceneGroup:insert(self.innerBar)
end 

--function to update the remaining health of player each time player is hit by enemy bullet
function stage:updateHealth( )
	--self.healthText.text = "Health: " .. self.gameState.playerHealth
	if(self.gameState.playerHealth >= 0) then 
		if (self.innerBar ~= nil) then
			self.innerBar:removeSelf()

			self.innerBar = display.newRect( 12, 12, self.gameState.playerHealth/self.totalHealth*96, 16 )
			self.innerBar.anchorX = 0
			self.innerBar.anchorY = 0
			self.innerBar:setFillColor( 0, 1, 0 )
		end 
	end 
end

local function createEnemy( data )
	local enemies = data.enemies
	local enemyTypes = data.enemyTypes
	local count = -1
	local rand = 0
	while count <= 0 do
		rand = math.random(1, enemyTypes)
		if enemies[rand]["count"] > 0 then
			enemies[rand]["count"] = enemies[rand]["count"] - 1
			count = enemies[rand]["count"]
			break
		end
	end
	local enemy = enemies[rand]
	local enemyData = {
		physics = physics,
		sceneGroup = data.sceneGroup,
		gameState = data.gameState,
		stage = data.stage,
	}
	if enemyYPosFlag then
		enemyData.yPos = 100
		enemyYPosFlag = false
	else
		enemyData.yPos = 200
		enemyYPosFlag = true
	end
	local enemyObject
	if (enemy.type == "helicopter") then
		enemyObject = helicopter:new(enemyData)
	end
	if (enemy.type == "boss") then
		enemyObject = boss:new(enemyData)
	end
	if (enemy.type == "enemy1") then
		enemyObject = enemy1:new(enemyData)
	end
	if (enemy.type == "enemy2") then
		enemyObject = enemy2:new(enemyData)
	end
	if (enemy.type == "enemy3") then
		enemyObject = enemy3:new(enemyData)
	end
	if (enemy.type == "enemy4") then
		enemyObject = enemy4:new(enemyData)
	end
	if (enemy.type == "enemy5") then
		enemyObject = enemy5:new(enemyData)
	end
	return enemyObject
end

--function to initialize enemy objects
function stage:initializeEnemy()
	local enemyCount = self.enemyCount
	local enemies = self.enemies
	local enemyTypes = self.enemyTypes
	local enemyInterval = self.enemyInterval
	local function showEnemey( enemyCount )
		print("enemy count is: ", enemyCount)
		local enemy = createEnemy({enemies = enemies, enemyTypes = enemyTypes, sceneGroup = self.sceneGroup, stage = self})
		self.activeEnemies[enemy] = enemy
		enemy:spawn()
		enemy:enter()
		enemyCount = enemyCount - 1
		if enemyCount > 0 and self.gameState.playerHealth > 0 then
			timer.performWithDelay( enemyInterval, function ( event )
					showEnemey(enemyCount)
			end )
		end
	end
	showEnemey(enemyCount)
end

--function to initialize player object 
function stage:initializePlayer()
	self.player = player:new({physics = physics, sceneGroup = self.sceneGroup, gameState = self.gameState,})
	self.player:spawn()
	self.player.pp = self
end

function stage:create( )
	print("Level: ", self.name)
	self:initializeStage()
end

function stage:touch(event)
	-- If touch started on the right side, ignore it
	if (event.xStart > display.contentCenterX) then
		return
	end

	local gun = self.player.gun

	if event.phase == "began" then
		gun.markX = event.xStart
		-- print("began touch")
	elseif event.phase == "moved" then
		-- print("touch moved")
		-- print(display.contentCenterX, display.contentCenterY)
		local angle = 2 * (event.x - gun.markX)/display.contentCenterX * 75
		gun.dispObj:rotate(-gun.rotation)
		gun.rotation = gun.rotation + angle
		gun:move()
		gun.markX = event.x
	elseif event.phase == "ended" then
		-- print("touch ended")
	end
end

function stage:tap(event)
	-- If tap was on left side ignore it
	if (event.x <= display.contentCenterX) then
		return
	end
	self.player:fire(event)
end

--function to remove the event listeners 
function stage:removeListeners()
	self.overlay:removeEventListener( "touch", self)
	self.overlay:removeEventListener( "tap", self)
end 

function stage:explodePlayer()
	self:removeListeners()
	self.player:explode()
end

function stage:cleanAllEnemies()
	for k,v in pairs(self.activeEnemies) do
		self.activeEnemies[k]:clean()
	end
end

--function to clean objects in stage after current play is over
function stage:clean()
	self.player:clean()
	self:cleanAllEnemies()
end 

return stage