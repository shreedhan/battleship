-- Enemy - Boss
local enemy = require("enemy")
local audioTable = require("audioTable")

local enemy3 = {
	name = "enemy3",
	health = 3,
	entrySound = audioTable.enemy3Enter,
	fireSound = audioTable.enemy3Fire,
	speed = 0.15,
	seqData = {{ name="normal", frames={5} } },
	outlineFrameIndex = 5,
	fireRate = 2,
}

enemy3 = enemy:new(enemy3)

return enemy3